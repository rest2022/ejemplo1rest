<?php

namespace app\controllers;

use app\models\Empleado;
use yii\rest\ActiveController;

class EmpleController extends ActiveController {

    public $modelClass = 'app\models\Empleado'; // colocamos el modelo de la tabla que vamos a utilizar

    public function actions() {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        //unset($actions['delete'], $actions['create']);
        // customize the data provider preparation with the "indexProvider" method
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];

        return $actions;
    }

    public function indexProvider() {

        // return Empleado::find()->all(); // se lo devuelvo como array de modelos

        return new \yii\data\ActiveDataProvider([// se lo devuelvo como dataProvider (mas correcto)
            'query' => Empleado::find()->where(["like","nombre","a%",false])
        ]);
    }

    public function actionEstadistica() {
        /* return new \yii\data\ActiveDataProvider([// se lo devuelvo como dataProvider (mas correcto)
          'query' => Empleado::find()->where("codigo>5")
          ]); */

        return \Yii::$app->db->createCommand("select codigo_departamento,count(*) total from empleado group by codigo_departamento")->queryAll();
    }

}

